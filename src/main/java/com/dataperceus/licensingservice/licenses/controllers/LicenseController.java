package com.dataperceus.licensingservice.licenses.controllers;

import com.dataperceus.licensingservice.licenses.models.License;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("v1/organisations/{organisationId}/licenses")
public class LicenseController {

    @RequestMapping("/")
    public License getLicenses() {
        return null;
    }
}
